package com.classpath.paymentservice.controller;

import com.classpath.paymentservice.model.MoneyTransfer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/v1/payments")
@Slf4j
public class PaymentController {

    @PostMapping
    public MoneyTransfer transferMoney(@RequestBody MoneyTransfer moneyTransfer){
        log.info("Successfully transfered money from {} to {} ::", moneyTransfer.getFrom(), moneyTransfer.getTo() );
        return moneyTransfer;
    }
}