package com.classpath.paymentservice.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
@AllArgsConstructor
public class MoneyTransfer {
    private String from;
    private String to;
    private double money;
    private String notes;
}